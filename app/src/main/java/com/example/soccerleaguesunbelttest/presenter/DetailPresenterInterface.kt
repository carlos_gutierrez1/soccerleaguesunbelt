package com.example.soccerleaguesunbelttest.presenter

interface DetailPresenterInterface {
    fun getTeamById(id_team: String)
}