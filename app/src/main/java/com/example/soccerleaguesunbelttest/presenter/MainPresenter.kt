package com.example.soccerleaguesunbelttest.presenter

import com.example.soccerleaguesunbelttest.data.model.League
import com.example.soccerleaguesunbelttest.data.model.TeamsResponse
import com.example.soccerleaguesunbelttest.interactor.MainInteractor
import com.example.soccerleaguesunbelttest.interactor.MainInteractorInterface
import com.example.soccerleaguesunbelttest.view.activities.MainViewInterface

class MainPresenter (val view: MainViewInterface): MainPresenterInterface, MainInteractorInterface {
    val interactor: MainInteractor = MainInteractor(this)
    override fun getTeamsByLeague(id_league: String) {
        interactor.getTeamsByLeague(id_league)
    }

    override fun getLeagueById(id_league: String) {
        interactor.getLeagueById(id_league)
    }

    override fun onSuccessTeams(teamsResponse : TeamsResponse) {
        view.setUpRecyclerView(teamsResponse)
    }

    override fun onError(message: String) {

    }

    override fun onSuccessLeague(league: League) {
        view.setUpHeader(league)
    }
}