package com.example.soccerleaguesunbelttest.presenter

import com.example.soccerleaguesunbelttest.data.model.Team
import com.example.soccerleaguesunbelttest.interactor.DetailInteractor
import com.example.soccerleaguesunbelttest.interactor.DetailInteractorInterface
import com.example.soccerleaguesunbelttest.view.activities.DetailViewInterface

class DetailPresenter(val view: DetailViewInterface) : DetailInteractorInterface,DetailPresenterInterface {
    val interactor: DetailInteractor = DetailInteractor(this)
    override fun getTeamById(id_team: String) {
        interactor.getTeamById(id_team)
    }

    override fun onSuccess(team: Team) {
        view.setUpView(team)
    }

    override fun onError(message: String) {
        TODO("Not yet implemented")
    }
}