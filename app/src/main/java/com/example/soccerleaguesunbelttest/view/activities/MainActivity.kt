package com.example.soccerleaguesunbelttest.view.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.soccerleaguesunbelttest.BaseApp
import com.example.soccerleaguesunbelttest.ID_SPANISH_LEAGUE
import com.example.soccerleaguesunbelttest.ID_TEAM
import com.example.soccerleaguesunbelttest.R
import com.example.soccerleaguesunbelttest.data.model.League
import com.example.soccerleaguesunbelttest.data.model.TeamsResponse
import com.example.soccerleaguesunbelttest.databinding.ActivityMainBinding
import com.example.soccerleaguesunbelttest.presenter.MainPresenter
import com.example.soccerleaguesunbelttest.presenter.MainPresenterInterface
import com.example.soccerleaguesunbelttest.view.adapter.TeamAdapter
import com.example.soccerleaguesunbelttest.view.dialogs.SelectLeagueDialog



class MainActivity : AppCompatActivity(), MainViewInterface, SelectLeagueDialog.SelectLeagueInterface{

    private lateinit var mApp: BaseApp
    private lateinit var idLeague: String
    private lateinit var binding: ActivityMainBinding
    private val presenter: MainPresenterInterface = MainPresenter(this)
    private val mDialogFilter: SelectLeagueDialog by lazy {
        SelectLeagueDialog(this)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Global
        mApp = applicationContext as BaseApp
        binding.mainActivityRecyclerView.setHasFixedSize(true)
        idLeague = ID_SPANISH_LEAGUE
        getData(idLeague)
        binding.mainActivityHeader.mainActivityFilterIcon.setOnClickListener { addFilter() }

    }

    private fun addFilter() {
        mDialogFilter.show()
        mDialogFilter.setCanceledOnTouchOutside(false)
    }

    private fun getData(id_league: String) {
        presenter.getTeamsByLeague(id_league)
        presenter.getLeagueById(id_league)
    }

    override fun setUpRecyclerView(listTeams: TeamsResponse) {
        var adapter = TeamAdapter(listTeams) {
            openDetailTeamActivity(it)
        }
        binding.mainActivityRecyclerView.adapter = adapter

    }

    private fun openDetailTeamActivity(idTeam: Int) {
        val intentDetailActivity = Intent(this, DetailTeamActivity::class.java)
        intentDetailActivity.putExtra(ID_TEAM, idTeam)
        startActivity(intentDetailActivity)
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.fade_out)

    }

    override fun setUpHeader(league: League) {
        binding.mainActivityHeader.mainActivityFilterIcon.visibility = View.VISIBLE
        binding.mainActivityHeader.mainActivityLeagueName.text = league.strLeague
        Glide.with(this)
            .load(league.strFanart1)
            .into(binding.mainActivityHeader.mainActivityLeagueImage)
    }

    override fun selectLeague(id_league: String) {
        getData(id_league)
    }
}