package com.example.soccerleaguesunbelttest.view.activities

import com.example.soccerleaguesunbelttest.data.model.League
import com.example.soccerleaguesunbelttest.data.model.TeamsResponse

interface MainViewInterface {
    fun setUpRecyclerView(teamsResponse: TeamsResponse)
    fun setUpHeader(league: League)

}
