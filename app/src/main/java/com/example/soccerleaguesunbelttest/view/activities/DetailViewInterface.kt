package com.example.soccerleaguesunbelttest.view.activities

import com.example.soccerleaguesunbelttest.data.model.Team

interface DetailViewInterface {
    fun setUpView(team: Team)
}