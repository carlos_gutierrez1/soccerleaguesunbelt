package com.example.soccerleaguesunbelttest.view.activities

import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.soccerleaguesunbelttest.ID_TEAM
import com.example.soccerleaguesunbelttest.R
import com.example.soccerleaguesunbelttest.data.model.Team
import com.example.soccerleaguesunbelttest.databinding.ActivityMainBinding
import com.example.soccerleaguesunbelttest.presenter.DetailPresenter
import com.example.soccerleaguesunbelttest.presenter.DetailPresenterInterface
import kotlinx.android.synthetic.main.activity_detail_team.*

class DetailTeamActivity : AppCompatActivity(), DetailViewInterface{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_team)

        // Toolbar
        setSupportActionBar(detailActivity_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        getData(intent.getIntExtra(ID_TEAM,1))

    }

    private fun getData(id_team: Int) {
        val presenter: DetailPresenterInterface = DetailPresenter(this)
        presenter.getTeamById(id_team.toString())

    }

    override fun setUpView(team: Team) {
        detailTeam_teamName.text = team.strTeam
        detailActivity_nameTeam.text = team.strTeam
        detailService_teamYear.text = team.intFormedYear.toString()
        detailService_teamDescription.text = team.strDescriptionEN
        Glide.with(this)
            .load(team.strTeamBadge)
            .into(detailActivity_logoTeam)
        Glide.with(this)
            .load(team.strTeamFanart1)
            .into(detailTeamBanner)
        Glide.with(this)
            .load(team.strTeamBadge)
            .into(detailTeam_logo)
        Glide.with(this)
            .load(team.strTeamJersey)
            .into(detailTeam_jersey)
    }

}