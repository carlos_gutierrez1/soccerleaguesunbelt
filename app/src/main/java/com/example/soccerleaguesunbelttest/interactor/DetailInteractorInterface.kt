package com.example.soccerleaguesunbelttest.interactor

import com.example.soccerleaguesunbelttest.data.model.Team

interface DetailInteractorInterface {
    fun onSuccess(action: Team)
    fun onError(message: String)
}