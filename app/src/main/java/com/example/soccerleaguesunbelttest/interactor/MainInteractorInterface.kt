package com.example.soccerleaguesunbelttest.interactor

import com.example.soccerleaguesunbelttest.data.model.League
import com.example.soccerleaguesunbelttest.data.model.TeamsResponse

interface MainInteractorInterface {
    fun onSuccessTeams(value: TeamsResponse)
    fun onError(message: String)
    fun onSuccessLeague(league: League)
}