package com.example.soccerleaguesunbelttest

import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.LifecycleObserver
import com.airbnb.lottie.BuildConfig
import timber.log.Timber
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

class BaseApp : Application(), LifecycleObserver {

    override fun attachBaseContext(context: Context) {
        super.attachBaseContext(context)

    }

    override fun onCreate() {
        super.onCreate()

        // Timber
        if (com.example.soccerleaguesunbelttest.BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        // Calligraphy
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
            .setDefaultFontPath("fonts/Nunito/Nunito-SemiBold.ttf")
            .setFontAttrId(R.attr.fontPath)
            .build()
        )

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }
}