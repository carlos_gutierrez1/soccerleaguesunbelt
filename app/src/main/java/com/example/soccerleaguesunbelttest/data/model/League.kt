package com.example.soccerleaguesunbelttest.data.model

class League (
    val idLeague : Int,
    val strLeague : String,
    val strLeagueAlternate : String,
    val strFanart1 : String,
    val strBanner : String,
    val strLogo : String
)
