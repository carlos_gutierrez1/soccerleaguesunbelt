package com.example.soccerleaguesunbelttest.data.model

class LeaguesResponse (
    val leagues: List<League>
)