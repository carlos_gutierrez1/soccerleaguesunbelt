package com.example.soccerleaguesunbelttest.data.api.retrofit

import com.example.soccerleaguesunbelttest.DETAIL_LEAGUE
import com.example.soccerleaguesunbelttest.TEAMS_SEARCH
import com.example.soccerleaguesunbelttest.TEAM_SEARCH
import com.example.soccerleaguesunbelttest.data.model.LeaguesResponse
import com.example.soccerleaguesunbelttest.data.model.TeamsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Interface with all the Web Services methods
 */
interface RetrofitService {

    @GET(TEAM_SEARCH)
    fun getTeamById(
        @Query ("id") id_team: String
    ): Single<TeamsResponse>

    @GET(TEAMS_SEARCH)
    fun getTeamsByLeague(
        @Query ("id") id_league: String
    ): Single<TeamsResponse>

    @GET(DETAIL_LEAGUE)
    fun getLeagueById(
        @Query ("id") id_league: String
    ): Single<LeaguesResponse>
}