package com.example.soccerleaguesunbelttest.data.api

import com.example.soccerleaguesunbelttest.data.model.LeaguesResponse
import com.example.soccerleaguesunbelttest.data.model.TeamsResponse
import io.reactivex.Single

interface ApiService {
    fun getTeamsByLeague(idLeague: String): Single<TeamsResponse>
    fun getLeagueById(idLeague: String): Single<LeaguesResponse>
    fun getTeamById(id_team: String): Single<TeamsResponse>
}