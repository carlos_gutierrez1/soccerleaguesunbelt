package com.example.soccerleaguesunbelttest.data.model

class TeamsResponse (
    val teams: List<Team>
)